package bestPoint2D.binaryChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;
import java.util.Scanner;


public class BPCrossover implements Recombination  {

	
	private float probability;
	
	public BPCrossover() {
		this.probability = 1;
	}
	
	public BPCrossover(float probability){
		this.probability = probability;
	}
	
	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		
		if (rnd.nextFloat()<this.probability) {
			String[] p1 = parents[0].toString().split(",");
			String[] p2 = parents[1].toString().split(",");

			
			
			

			int part1 = rnd.nextInt(parents[0].getNumGenes());
			
			String rep1 = p1[0].substring(0, part1) + p2[0].substring(part1);
			String rep2 = p2[0].substring(0, part1) + p1[0].toString().substring(part1);
			
			part1 = rnd.nextInt(parents[0].getNumGenes());
			rep1 += ","+ p1[1].substring(0, part1) + p2[1].substring(part1);
			rep2 += ","+ p2[1].substring(0, part1) + p1[1].toString().substring(part1);
			
			
			children[0] = factory.createChromosome(rep1);
			children[1] = factory.createChromosome(rep2);
		}else{
			children[0] = parents[0];
			children[1] = parents[1];
		}
		
		


	}

}
