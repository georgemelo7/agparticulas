package bestPoint2D.binaryChromosome;

import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.EAStatisticDecorator;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.GeneticAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.mutationBehaviors.BinaryBitPerBitMutation;
import evolucionaryAlgorithm.mainAlgorithm.selectionMethods.TournamentSelection;
import evolucionaryAlgorithm.mainAlgorithm.terminateConditions.NeverEnds;

public class Teste {
	
	
	
	public static void main(String[] args) {
		EvolucionaryAlgorithm ga = new EAStatisticDecorator(new GeneticAlgorithm());
		
		ga.setFitnessMethod(new BPFitnessCalc());
		ga.setSelectionMethod(new TournamentSelection(5));
		ga.setPopulFactory(new BPFactory(7));
		ga.setRecombination(new BPCrossover());
		ga.setMutationBehavior(new BinaryBitPerBitMutation(0.08f));
		ga.setTerminateCondition(new NeverEnds());
		
		Object[] point = {34, 78};
		
		
		
		ga.initPopulation(30);
		
		for (int i = 0; i < 100; i++) {
			ga.selectChromosomes(point);
		}
                
	}

}
