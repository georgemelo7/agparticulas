package bestPoint2D.binaryChromosome;

import evolucionaryAlgorithm.interfaces.Chromosome;

public class BinaryPairChromosome extends Chromosome {

	private volatile String genes1;
	private volatile String genes2;
	private final int NUM_GENES;
	
	public BinaryPairChromosome(String bitsPair){
		setGenes(bitsPair);
		this.NUM_GENES = genes1.length();
	}
	
	
	@Override
	public int getNumGenes() {
		return this.NUM_GENES;
	}

	@Override
	public void setGenes(String bits) {
		String[] b = bits.split(",");
		if(b.length == 2 && b[0].length() == b[1].length()){
			this.genes1 = b[0];
			this.genes2 = b[1];
		}
	}
	
	@Override
	protected void clearChromosome() {
		this.genes1 = "";
		this.genes2 = "";
	}

	@Override
	public String toString() {
		return this.genes1+","+this.genes2;
	}

}