package bestPoint2D.binaryChromosome;

import java.util.Random;


import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class BPFactory implements ChromosomeFactory {

	private int numGenes;
	
	public BPFactory(int numGenes) {
		this.numGenes = numGenes;
	}
	
	@Override
	public Chromosome[] createInitalPopulation(int numChromosome) {
		Chromosome[] chromosomes = new Chromosome[numChromosome];
		for(int i=0; i<numChromosome; i++){
			chromosomes[i] = this.createRandomChromosome();
		}
		return chromosomes;
	}

	@Override
	public Chromosome[] createVoidPopulation(int numChromosome) {
		Chromosome[] chrs = new BinaryPairChromosome[numChromosome];
		for (int i = 0; i < chrs.length; i++) {
			chrs[i] = this.createChromosome("0,0");
		}
		return chrs;
	}

	@Override
	public Chromosome createRandomChromosome() {
		Random randomGenerator = RandomGenerator.getRandomGenerator();
		StringBuilder bits = new StringBuilder();
		for(int i=0; i<this.numGenes; i++){
			if(randomGenerator.nextDouble() >= 0.5){
				bits.append("1");
			}else{
				bits.append("0");
			}
		}
		bits.append(",");
		for(int i=0; i<this.numGenes; i++){
			if(randomGenerator.nextDouble() >= 0.5){
				bits.append("1");
			}else{
				bits.append("0");
			}
		}
		return new BinaryPairChromosome(bits.toString());
	}

	@Override
	public Chromosome createChromosome(String str) {
		return new BinaryPairChromosome(str);
	}

	@Override
	public void clearChromosomes(Chromosome[] chromosomes) {
		for (Chromosome chromosome : chromosomes) {
			chromosome.clear();
		}
	}

}
