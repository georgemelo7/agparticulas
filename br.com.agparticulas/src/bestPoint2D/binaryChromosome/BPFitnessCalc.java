package bestPoint2D.binaryChromosome;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.FitnessMethod;

public class BPFitnessCalc implements FitnessMethod {

	@Override
	public int calcFitness(Chromosome chromosome, Object ... params) {

		int x = 0;
		int y = 0;

		try {
			x = (int) params[0];
			y = (int) params[1];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Parâmetros vazios");
		}

		
		String[] chr = chromosome.toString().split(",");
		
		
		int fitness = 0;
		try {
			fitness += 100 - Math.abs(x-Integer.parseInt(chr[0], 2));
			fitness += 100 - Math.abs(y-Integer.parseInt(chr[1], 2));
		
		} catch (NumberFormatException e) {
			throw new NumberFormatException("Cromossomo não binário");
		}catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Cromossomo não é um par");
		}
		
		

		return fitness;

	}

}
