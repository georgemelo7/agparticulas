package bestPoint2D.floatChromosome;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.interfaces.FitnessMethod;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.interfaces.SurvivalMethod;
import evolucionaryAlgorithm.interfaces.TerminateCondition;

public class EPSO implements EvolucionaryAlgorithm {

	
	private ChromosomeFactory factory;
	private FitnessMethod fitnessMethod;
	private SelectionMethod selectionMethod;
	private Recombination recombination;
	private MutationBehavior mutationBehavior;
	private SurvivalMethod survivalMethod;
	private TerminateCondition terminateCondition;
	private Chromosome[] population;
	private Chromosome[] selected;
	private Chromosome[] chromosomePair;
	private Chromosome[] children;
	
	public static Chromosome best;
	public static Chromosome avg;
	
	
	
	@Override
	public void initPopulation(int numChromosome, Object... phenotype) {
		this.selected =  this.factory.createInitalPopulation(numChromosome);
		this.population = this.factory.createVoidPopulation(numChromosome);
		this.chromosomePair = this.factory.createVoidPopulation(2);
		this.children = this.factory.createVoidPopulation(2);
		for (int i = 0; i < this.selected.length; i++) {
			this.fitnessMethod.calcFitness(selected[i], phenotype);
		}
		EPSO.best = this.factory.createChromosome(this.selected[0].toString());
		EPSO.avg = this.factory.createChromosome(this.selected[0].toString());
	}

	@Override
	public void selectChromosomes(Object... phenotype) {
		
		/*
		 * Copiando os selecionados da geração passada para a 
		 * população da nova geração
		 */
		int maior = 0;
		for (int i = 0; i < this.population.length; i++) {
			this.population[i].setGenes(this.selected[i].toString());
			this.population[i].setFitness(this.selected[i].getFitness());
			this.population[i].setLikelihood(this.selected[i].getLikelihood());
			if(this.population[i].compareTo(this.population[maior]) == 1){
				maior = i;
			}
		}
		EPSO.best.setGenes(this.population[maior].toString());
		calcMean(population);
		
		
		/*
		 * Realiza a recombinação e mutação com a população
		 */
		for(int i = 0; i < population.length; i++){
			
			this.selectionMethod.select(this.population, this.chromosomePair);	
			this.recombination.recombination(this.chromosomePair, this.children, this.factory);
			this.selected[i].setGenes(this.children[0].toString());
			this.mutationBehavior.mutate(this.selected[i]);
			
			try{
				i++;
				this.selected[i].setGenes(children[1].toString());
				this.mutationBehavior.mutate(this.selected[i]);
				
			}catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("ArrayIndexOutOfBoundsException");
				break;
			}
		}
		
		/*
		 * Calcula o fitness dos selecionados
		 */
		for (int i = 0; i < selected.length; i++) {
			this.fitnessMethod.calcFitness(selected[i],phenotype);
		}
		
		/*
		 * Realiza a sobrevivência da população
		 */
		this.survivalMethod.survive(this.selected, this.population);
	}

	@Override
	public boolean verifyTerminateCondition() {
		return this.terminateCondition.isTerminate(this.population);
	}

	
	private void calcMean(Chromosome[] popul){
		float x = 0, y = 0;
		String[] xy;
		for (int i = 0; i < popul.length; i++) {
			xy = popul[i].toString().split(",");
			x += Float.parseFloat(xy[0]);
			y += Float.parseFloat(xy[1]);
		}
		EPSO.avg.setGenes(x/popul.length + "," + y/popul.length);
	}
	
	
	public SurvivalMethod getSurvivalMethod() {
		return this.survivalMethod;
	}
	public void setSurvivalMethod(SurvivalMethod survivalMethod) {
		this.survivalMethod = survivalMethod;
	}
	public ChromosomeFactory getPopulFactory() {
		return this.factory;
	}
	public void setPopulFactory(ChromosomeFactory populFactory) {
		this.factory = populFactory;
	}
	public TerminateCondition getTerminateCondition() {
		return this.terminateCondition;
	}
	public void setTerminateCondition(TerminateCondition terminateCondition) {
		this.terminateCondition = terminateCondition;
	}
	public FitnessMethod getFitnessMethod() {
		return this.fitnessMethod;
	}
	public void setFitnessMethod(FitnessMethod fitnessMethod) {
		this.fitnessMethod = fitnessMethod;
	}
	public SelectionMethod getSelectionMethod() {
		return this.selectionMethod;
	}
	public void setSelectionMethod(SelectionMethod selectionMethod) {
		this.selectionMethod = selectionMethod;
	}
	public Recombination getRecombination() {
		return this.recombination;
	}
	public void setRecombination(Recombination recombination) {
		this.recombination = recombination;
	}
	public MutationBehavior getMutationBehavior() {
		return this.mutationBehavior;
	}
	public void setMutationBehavior(MutationBehavior mutationBehavior) {
		this.mutationBehavior = mutationBehavior;
	}
	public Chromosome[] getPopulation() {
		return this.population;
	}
	public Chromosome[] getSelected() {
		return this.selected;
	}

}
