package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class GaussianMutation implements MutationBehavior {

	private float SDeviation;
	private float probability;
	
	public GaussianMutation(float probability, float SDeviation) {
		this.SDeviation = SDeviation;
		this.probability = probability;
		
	}
	
	
	@Override
	public void mutate(Chromosome chromosome) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		if(rnd.nextFloat() < this.probability){
			String[] xY = chromosome.toString().split(",");
			
			float x = (float) Math.min(1,Math.max(0, Float.parseFloat(xY[0]) + this.SDeviation*rnd.nextGaussian()));
			float y = (float) Math.min(1,Math.max(0, Float.parseFloat(xY[1]) + this.SDeviation*rnd.nextGaussian()));
			
			chromosome.setGenes(x+","+y);
		}
		
	}

}
