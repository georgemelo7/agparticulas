package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class FloatWholeEPSO2 implements Recombination {

	private float probability;
	private float a1, a2, a3;
	
	public FloatWholeEPSO2(float a1, float a2, float a3) {
		this.probability = 1;
		this.a1 = a1;
		this.a2 = a2;
		this.a3 = a3;
	}
	
	public FloatWholeEPSO2(float probability, float a1, float a2, float a3) {
		this.probability = probability;
		this.a1 = a1;
		this.a2 = a2;
		this.a3 = a3;
		
	}
	
	
	
	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory) {
		Random rnd = RandomGenerator.getRandomGenerator();
		
		if(rnd.nextFloat() < this.probability){
			
			float alphaX = rnd.nextFloat();
			float alphaY = rnd.nextFloat();
			float x, xP, y, yP, xC1, yC1, xC2, yC2;
			
			String[] xY = parents[0].toString().split(","); 
			x = Float.parseFloat(xY[0]);
			y = Float.parseFloat(xY[1]);
			
			xY = parents[1].toString().split(",");
			xP = Float.parseFloat(xY[0]);
			yP = Float.parseFloat(xY[1]);
			
			xC1 = alphaX*x + (1 - alphaX)*xP;
			yC1 = alphaY*y + (1 - alphaY)*yP;
			
			xC2 = alphaX*xP + (1 - alphaX)*x;
			yC2 = alphaY*yP + (1 - alphaY)*y;
			
			
			//Calculando com a média e o melhor da população
			xY = EPSO.avg.toString().split(",");
			float xAvg = Float.parseFloat(xY[0]);
			float yAvg = Float.parseFloat(xY[1]);
			
			xY = EPSO.best.toString().split(",");
			float xBest = Float.parseFloat(xY[0]);
			float yBest = Float.parseFloat(xY[1]);
			
			
			xC1 = this.a1*xC1 + this.a2*xAvg + this.a3*xBest;
			yC1 = this.a1*yC1 + this.a2*yAvg + this.a3*yBest;
			
			xC2 = this.a1*xC2 + this.a2*xAvg + this.a3*xBest;
			yC2 = this.a1*yC2 + this.a2*yAvg + this.a3*yBest;
			
			

			children[0].setGenes(xC1 + "," + yC1);
			children[1].setGenes(xC2 + "," + yC2);
			
			
		}else{
			children[0].setGenes(parents[0].toString());
			children[1].setGenes(parents[1].toString());
		}
	}

}
