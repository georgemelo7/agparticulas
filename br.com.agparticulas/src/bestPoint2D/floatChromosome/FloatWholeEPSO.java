package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class FloatWholeEPSO implements Recombination {

	
	private float probability;
	
	public FloatWholeEPSO() {
		this.probability = 1;
	}
	
	public FloatWholeEPSO(float probability) {
		this.probability = probability;
	}
	
	
	
	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory) {
		Random rnd = RandomGenerator.getRandomGenerator();
		
		if(rnd.nextFloat() < this.probability){
			
			float alphaX = rnd.nextFloat();
			float alphaY = rnd.nextFloat();
			float x, xP, y, yP, xC1, yC1, xC2, yC2;
			
			String[] xY = parents[0].toString().split(","); 
			x = Float.parseFloat(xY[0]);
			y = Float.parseFloat(xY[1]);
			
			xY = parents[1].toString().split(",");
			xP = Float.parseFloat(xY[0]);
			yP = Float.parseFloat(xY[1]);
			
			xC1 = alphaX*x + (1 - alphaX)*xP;
			yC1 = alphaY*y + (1 - alphaY)*yP;
			
			xC2 = alphaX*xP + (1 - alphaX)*x;
			yC2 = alphaY*yP + (1 - alphaY)*y;
			
			
			//---- Cruzamento com a média da população
			alphaX = rnd.nextFloat();
			alphaY = rnd.nextFloat();
			
			xY = EPSO.avg.toString().split(",");
			xP = Float.parseFloat(xY[0]);
			yP = Float.parseFloat(xY[1]);
			
			xC1 = alphaX*xC1 + (1 - alphaX)*xP;
			yC1 = alphaY*yC1 + (1 - alphaY)*yP;
			
			xC2 = alphaX*xP + (1 - alphaX)*xC2;
			yC2 = alphaY*yP + (1 - alphaY)*yC2;
			
			
			
			
			//---- Cruzamento com o melhor gene da população
			alphaX = rnd.nextFloat();
			alphaY = rnd.nextFloat();
			
			xY = EPSO.best.toString().split(",");
			xP = Float.parseFloat(xY[0]);
			yP = Float.parseFloat(xY[1]);
			
			xC1 = alphaX*xC1 + (1 - alphaX)*xP;
			yC1 = alphaY*yC1 + (1 - alphaY)*yP;
			
			xC2 = alphaX*xP + (1 - alphaX)*xC2;
			yC2 = alphaY*yP + (1 - alphaY)*yC2;
			
			

			children[0].setGenes(xC1 + "," + yC1);
			children[1].setGenes(xC2 + "," + yC2);
			
			
		}else{
			children[0].setGenes(parents[0].toString());
			children[1].setGenes(parents[1].toString());
		}
	}

}
