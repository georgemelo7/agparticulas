package bestPoint2D.floatChromosome;


import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.FitnessMethod;

public class FloatFitnessMethod implements FitnessMethod {

	@Override
	public int calcFitness(Chromosome chromosome, Object... params) {
		
		float x = 0;
		float y = 0;
		
		
		try {
			x = (float) params[0];
			y = (float) params[1];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Parâmetros vazios");
		}
		
		
		
		String[] chr = chromosome.toString().split(",");
		
		chromosome.setLikelihood( 2 - (Math.abs(x - Float.parseFloat(chr[0])) +
				Math.abs(y - Float.parseFloat(chr[1])))    );
		
//		System.out.println("l: "+ chromosome.getLikelihood() +"  x: "+ (Float.parseFloat(chromosome.toString().split(",")[0])*500) + 
//				"  y: " + (Float.parseFloat(chromosome.toString().split(",")[1])*500) );
		
		
		return 0;
	}

}
