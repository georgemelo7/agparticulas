package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class FloatWholeArithmetic implements Recombination {

	
	private float probability;
	
	public FloatWholeArithmetic() {
		this.probability = 1;
	}
	
	public FloatWholeArithmetic(float probability) {
		this.probability = probability;
	}
	
	
	
	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		
		if(rnd.nextFloat() < this.probability){
			
			float alphaX = rnd.nextFloat();
			float alphaY = rnd.nextFloat();
			float x, xP, y, yP;
			
			String[] xY = parents[0].toString().split(","); 
			x = Float.parseFloat(xY[0]);
			y = Float.parseFloat(xY[1]);
			
			xY = parents[1].toString().split(",");
			xP = Float.parseFloat(xY[0]);
			yP = Float.parseFloat(xY[1]);
			
			
			children[0].setGenes((alphaX*x + (1 - alphaX)*xP)  + "," + (alphaY*y + (1 - alphaY)*yP));
			children[1].setGenes((alphaX*xP + (1 - alphaX)*x)  + "," + (alphaY*yP + (1 - alphaY)*y));
			
			
		}else{
			children[0].setGenes(parents[0].toString());
			children[1].setGenes(parents[1].toString());
		}
		
	}
	
}
