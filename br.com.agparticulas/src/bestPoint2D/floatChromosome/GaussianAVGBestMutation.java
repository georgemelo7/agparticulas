package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class GaussianAVGBestMutation implements MutationBehavior {

	private float SDeviation;
	private float probability;
	private float a1, a2, a3;
	
	public GaussianAVGBestMutation(float probability, float SDeviation,
			float a1, float a2, float a3) {
		this.SDeviation = SDeviation;
		this.probability = probability;
		this.a1 = a1;
		this.a2 = a2;
		this.a3 = a3;
	}
	
	
	@Override
	public void mutate(Chromosome chromosome) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		if(rnd.nextFloat() < this.probability){
			String[] xY = chromosome.toString().split(",");
			
			float x = (float) Math.min(1,Math.max(0, Float.parseFloat(xY[0]) + this.SDeviation*rnd.nextGaussian()));
			float y = (float) Math.min(1,Math.max(0, Float.parseFloat(xY[1]) + this.SDeviation*rnd.nextGaussian()));
			
			xY = EPSO.avg.toString().split(",");
			float xAvg = Float.parseFloat(xY[0]);
			float yAvg = Float.parseFloat(xY[1]);
			
			xY = EPSO.best.toString().split(",");
			float xBest = Float.parseFloat(xY[0]);
			float yBest = Float.parseFloat(xY[1]);
			
			
			x = this.a1*x + this.a2*xAvg + this.a3*xBest;
			y = this.a1*y + this.a2*yAvg + this.a3*yBest;
			
			
			
			chromosome.setGenes(x+","+y);
		}
		
	}
}
