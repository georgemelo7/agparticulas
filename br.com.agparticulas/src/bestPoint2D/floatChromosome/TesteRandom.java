package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class TesteRandom {
	
	
	public static void main(String[] args) {
		
		
		
		Random rnd = RandomGenerator.getRandomGenerator();
		for (int i = 0; i < 100; i++) {
			System.out.println(Math.min(1,Math.max(0, 0.1 + 0.25*rnd.nextGaussian()   )));
		}
		
		
	}

}
