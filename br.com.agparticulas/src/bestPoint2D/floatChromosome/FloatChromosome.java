package bestPoint2D.floatChromosome;

import evolucionaryAlgorithm.interfaces.Chromosome;

public class FloatChromosome extends Chromosome {

	private float x;
	private float y;
	
	
	public FloatChromosome(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public int getNumGenes() {
		return 2;
	}

	@Override
	public void setGenes(String bits) {
		String[] xY = bits.split(",");
		this.x = Float.parseFloat(xY[0]);
		this.y = Float.parseFloat(xY[1]);
	}

	@Override
	public String toString() {
		return this.x+","+this.y;
	}

	@Override
	protected void clearChromosome() {
		this.x = 0;
		this.y = 0;
	}

	@Override
	public int compareTo(Chromosome o) {
		if(this.likelihood < o.getLikelihood()){
			return -1;
		}else if (this.likelihood > o.getLikelihood()){
			return 1;
		}
		return 0;
	}
	
}
