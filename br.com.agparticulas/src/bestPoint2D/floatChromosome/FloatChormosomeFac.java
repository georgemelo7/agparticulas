package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class FloatChormosomeFac implements ChromosomeFactory{

	@Override
	public Chromosome[] createInitalPopulation(int numChromosome) {
		
		Chromosome[] chrs = new FloatChromosome[numChromosome];
		for (int i = 0; i < numChromosome; i++) {
			chrs[i] = this.createRandomChromosome();
		}
		return chrs;
	}

	@Override
	public Chromosome[] createVoidPopulation(int numChromosome) {
		Chromosome[] chrs = new FloatChromosome[numChromosome];
		for (int i = 0; i < numChromosome; i++) {
			chrs[i] = this.createChromosome("0,0");
		}
		return chrs;
	}

	@Override
	public Chromosome createRandomChromosome() {
		Random rnd = RandomGenerator.getRandomGenerator();
		return new FloatChromosome(rnd.nextFloat(), rnd.nextFloat());
	}

	@Override
	public Chromosome createChromosome(String str) {
		String[] xY = str.split(",");
		return new FloatChromosome(Float.parseFloat(xY[0]), Float.parseFloat(xY[1]));
	}

	@Override
	public void clearChromosomes(Chromosome[] chromosomes) {
		for (Chromosome chromosome : chromosomes) {
			chromosome.clear();
		}
	}

	
	
	
}
