package bestPoint2D.floatChromosome;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class RouletteWhell implements SelectionMethod {

	
	
	@Override
	public void select(Chromosome[] population, Chromosome[] voidSelected) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		
		float total = 0, sum = 0, random;
		
		
		random = rnd.nextFloat();
		
		for (Chromosome chromosome : population) {
			total += chromosome.getLikelihood();
		}
		
		for(Chromosome chromosome :population){
			sum += chromosome.getLikelihood()/total;
			if(sum>random){
				voidSelected[0].setGenes(chromosome.toString());
				break;
			}
		}
		
		random = rnd.nextFloat();
		sum = 0;
		
		for(Chromosome chromosome :population){
			sum += chromosome.getLikelihood()/total;
			if(sum>random){
				voidSelected[1].setGenes(chromosome.toString());
				break;
			}
		}
		
		
	}

}
