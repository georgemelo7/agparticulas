package eightQueen;

import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.EAStatisticDecorator;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.GeneticAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.selectionMethods.TournamentSelection;


public class EQTest {
	
	public static void main(String[] args) {
		
		EvolucionaryAlgorithm ga = new EAStatisticDecorator(new GeneticAlgorithm()) ;
		
		ga.setPopulFactory(new EQFactoryChromosome(8));
		ga.setFitnessMethod(new EQFitnessCalc());
		ga.setSelectionMethod(new TournamentSelection(5));
		ga.setRecombination(new EQCrossover());
		ga.setMutationBehavior(new EQSwapMutation(0.8f));
		ga.setTerminateCondition(new EQTerminateCondition(100));
		
		
		ga.initPopulation(10);
		
		for(int i=0; !ga.verifyTerminateCondition(); i++){
			System.out.println("\n------------- Geração "+i+" -----------------");
			ga.selectChromosomes();
		}
		
		
	}

}
