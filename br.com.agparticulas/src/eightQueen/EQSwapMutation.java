package eightQueen;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.randomGenerator.MersenneTwister;


public class EQSwapMutation implements MutationBehavior {


	private float probabily;

	public EQSwapMutation(float probabily) {
		this.probabily = probabily;
	}



	@Override
	public void mutate(Chromosome chromosome) {

		Random rnd = new MersenneTwister();
		

		if(rnd.nextFloat() < this.probabily){
			StringBuffer chromo = new StringBuffer(chromosome.toString());
			String newChromosome;
			int len = chromo.length();
			
			int gene1, gene2;
			char aux;

			gene1 = rnd.nextInt(len);
			gene2 = gene1;
			while(gene2 == gene1){
				gene2 = rnd.nextInt(len);
			}
			aux = chromo.charAt(gene1);
			chromo.setCharAt(gene1, chromo.charAt(gene2));
			chromo.setCharAt(gene2, aux);
			newChromosome = chromo.toString();
			chromosome.setGenes(newChromosome);
		}

		
	}


	@Override
	public String toString() {
		return "Swap Mutation com "+this.probabily+" de probabilidade por cromossomo.";
	}

}
