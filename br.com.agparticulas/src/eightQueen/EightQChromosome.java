package eightQueen;

import evolucionaryAlgorithm.interfaces.Chromosome;

public class EightQChromosome extends Chromosome {

	private int[] genes;
	
	public EightQChromosome(String genes) {
		this.setGenes(genes);
	}
	
	
	@Override
	public int getNumGenes() {
		return genes.length;
	}

	@Override
	public void setGenes(String genes) {
		int genesCount = genes.length();
		this.genes = new int[genesCount];
		for (int i = 0; i < genesCount; i++) {
			this.genes[i] = Integer.parseInt(String.valueOf(genes.charAt(i)));
		}
		
	}
	
	@Override
	protected void clearChromosome() {
		int num = this.genes.length;
		this.genes = new int[num];
	}

	@Override
	public String toString() {
		StringBuffer genes = new StringBuffer();
		for (int i = 0; i < this.genes.length; i++) {
			genes.append(this.genes[i]);
		}
		return genes.toString();
	}
	
	

}
