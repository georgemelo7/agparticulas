package eightQueen;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class EQFactoryChromosome implements ChromosomeFactory{

	private int numGenes;
	
	public EQFactoryChromosome(int numGenes) {
		this.numGenes = numGenes;
	}
	
	
	@Override
	public Chromosome[] createInitalPopulation(int numChromosome) {
		Chromosome[] chromosomes = new EightQChromosome[numChromosome];
		for (int i = 0; i < chromosomes.length; i++) {
			chromosomes[i] = this.createRandomChromosome();
		}
		return chromosomes;
	}

	@Override
	public Chromosome[] createVoidPopulation(int numChromosome) {
		return new EightQChromosome[numChromosome];
	}

	@Override
	public Chromosome createRandomChromosome() {
		String str = "";
		int gene;
		Random random = RandomGenerator.getRandomGenerator();
		for (int i = 0; i < this.numGenes; i++) {
			gene = random.nextInt(8);
			while(str.contains(gene+"")){
				gene = random.nextInt(8);
			}
			str += gene;
		}
		return new EightQChromosome(str);
	}

	@Override
	public Chromosome createChromosome(String str) {
		return new EightQChromosome(str);
	}
	
	@Override
	public void clearChromosomes(Chromosome[] chromosomes) {
		for (Chromosome chromosome : chromosomes) {
			chromosome.clear();
		}
	}

}
