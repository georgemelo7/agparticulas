package eightQueen;

import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.chormosomeFactories.BinaryFactory;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.EAStatisticDecorator;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.GeneticAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.fitnessMethods.FitnessTest;
import evolucionaryAlgorithm.mainAlgorithm.mutationBehaviors.BinaryBitPerBitMutation;
import evolucionaryAlgorithm.mainAlgorithm.recombinations.SimpleCrossover;
import evolucionaryAlgorithm.mainAlgorithm.selectionMethods.TournamentSelection;
import evolucionaryAlgorithm.mainAlgorithm.terminateConditions.BestFitness;

public class SGATest {
	
	public static void main(String[] args) {
		
		EvolucionaryAlgorithm ga = new EAStatisticDecorator(new GeneticAlgorithm()) ;
		
		ga.setPopulFactory(new BinaryFactory(10));
		ga.setFitnessMethod(new FitnessTest());
		ga.setSelectionMethod(new TournamentSelection(5));
		ga.setRecombination(new SimpleCrossover());
		ga.setMutationBehavior(new BinaryBitPerBitMutation(0.1f));
		ga.setTerminateCondition(new BestFitness(1023));
		
		
		ga.initPopulation(10);
		
		for(int i=0; !ga.verifyTerminateCondition(); i++){
			System.out.println("\n------------- Geração "+i+" -----------------");
			ga.selectChromosomes();
		}
	}

}
