package eightQueen;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.TerminateCondition;

public class EQTerminateCondition implements TerminateCondition {

	
	private final int MAX_LOOPS;
	private int count;
	
	public EQTerminateCondition() {
		this.MAX_LOOPS = -1;
	}
	
	public EQTerminateCondition(int maxLoops){
		this.MAX_LOOPS = maxLoops;
		this.count = 0;
	}
	
	
	@Override
	public boolean isTerminate(Chromosome[] population) {
		
		
		
		for (int i = 0; i < population.length; i++) {
			if(population[i].getFitness() == 28){
				System.out.println("Solução Encontrada: "+ population[i].toString());
				return true;
			}
		}
		
		
		if(this.MAX_LOOPS == -1){
			
			return false;
		}else if( this.count < this.MAX_LOOPS){
			this.count++;
			return false;
		}else{
			System.out.println("Número máximo de loops!!");
			return true;
		}
		
		
		
	}

	@Override
	public String toString() {
		
		return "Solução ou "+this.MAX_LOOPS+" loops.";
	}
	
}
