package eightQueen;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.FitnessMethod;

public class EQFitnessCalc implements FitnessMethod {

	@Override
	public int calcFitness(Chromosome chromosome, Object ... params) {
		String chromStr = chromosome.toString();
		int chromoCont = chromStr.length();
		int[] chromo = new int[chromoCont];
		int fitness = 0;
		for (int i = 0; i < chromoCont; i++) {
			chromo[i] = Integer.parseInt(chromStr.substring(i, i+1));
		}
		for (int i = 0; i < chromo.length; i++) {
			for (int j = i+1; j < chromo.length; j++) {
				if(Math.abs(i-j) == Math.abs(chromo[i]-chromo[j])){
					fitness++;
				}
			}
		}
		return 28-fitness;
	}
	
	@Override
	public String toString() {
		return "Máximo de Erros Menos o Número de Erros";
	}

}
