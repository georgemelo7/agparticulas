package eightQueen;

import java.security.SecureRandom;
import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;


public class EQCrossover implements Recombination {

	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory) {


		Random rnd = new SecureRandom();
		
		String cabeca0, cauda0, cabeca1, cauda1;
//		int len = parents[0].toString().length();

		if(parents[0].getNumGenes() == parents[0].getNumGenes()){
			int part = rnd.nextInt(parents[0].getNumGenes()-4) + 2;
			cabeca0 = parents[0].toString().substring(0, part);
			cauda0 = parents[0].toString().substring(part);
			cabeca1 = parents[1].toString().substring(0, part);
			cauda1 = parents[1].toString().substring(part);
			
			for (int i = 0; i < cauda1.length(); i++) {
				if(cabeca0.contains(cauda1.charAt(i)+"")){
					cabeca1 += cauda1.charAt(i);
				}else{
					cabeca0 += cauda1.charAt(i);
				}
			}
			for (int i = 0; i < cauda0.length(); i++) {
				if(cabeca1.contains(cauda0.charAt(i)+"")){
					cabeca0 += cauda0.charAt(i);
				}else{
					cabeca1 += cauda0.charAt(i);
				}
			}
			
//			System.out.println("rep1: "+re);
			
			children[0] = factory.createChromosome(cabeca0);
			children[1] = factory.createChromosome(cabeca1);

		}
	}


	@Override
	public String toString() {
		return "Crossover Escapando Genes Repetidos";
	}


}
