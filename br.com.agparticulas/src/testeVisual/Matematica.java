package testeVisual;

import java.awt.Point;

public final class Matematica {


	private Matematica() {
	}

	public static int[] calculaReta(int x1, int y1, int x2, int y2, int imgLargura, int imgAltura) {
		
		int[] xYResultados = new int[4];
		
		float a = (float) (x1 + (imgLargura / 2.0));
		float b = (float) (y1 + (imgAltura / 2.0));
		float a2 = (float) (x2 + (imgLargura / 2.0));
		float b2 = (float) (y2 + (imgAltura / 2.0));
		double angulo = Math.atan(Math.abs(b2- b) / Math.abs(a2 -a));
		float auxY = (float) ((imgAltura / 2.0) * Math.sin(angulo));
		float auxX = (float) ((imgLargura / 2.0) * Math.cos(angulo));

		if (b > b2) {
			xYResultados[1] = Math.round(b - auxY);
			xYResultados[3] = Math.round(b2 + auxY);
			
		} else {
			xYResultados[1] = Math.round(b + auxY);
			xYResultados[3] = Math.round(b2 - auxY);

		}
		if (a > a2) {
			xYResultados[0] = Math.round(a - auxX);
			xYResultados[2] = Math.round(a2 + auxX);

		} else {
			xYResultados[0] = Math.round(a + auxX);
			xYResultados[2] = Math.round(a2 - auxX);

		}
		return xYResultados;

	}
	
	/**
	 * Dado os pontos de dois objetos no plano, esse m�todo calcula
	 * e retorna dois pontos para se desenhar uma reta entre os objetos. Essa reta aponta
	 * para o centro dos objetos.<br />
	 * 
	 * @param origem Ponto do objeto de origem da seta.
	 * @param destino Ponto do objeto de destino da seta.
	 * @param objLargura Largura do objeto.
	 * @param objAltura Altura do objeto.
	 * @return ponto[<b>0</b>] -> novaOrigem;<br /> ponto[<b>1</b>] -> novoDestino.
	 */
	public static Point[] calculaReta(Point origem, Point destino, int objLargura, int objAltura){
		final int TAM = 2;
		Point[] xYResultados = new Point[TAM];
		
		float a = (float) (origem.getX() + (objLargura / 2.0));
		float b = (float) (origem.getY() + (objAltura / 2.0));
		float a2 = (float) (destino.getX() + (objLargura / 2.0));
		float b2 = (float) (destino.getY() + (objAltura / 2.0));
		double angulo = Math.atan(Math.abs(b2- b) / Math.abs(a2 -a));
		float auxY = (float) ((objAltura / 2.0) * Math.sin(angulo));
		float auxX = (float) ((objLargura / 2.0) * Math.cos(angulo));
		
		for(int i = 0; i <TAM; i++) xYResultados[i] = new Point();

		if (b > b2) {
			xYResultados[0].y = Math.round(b - auxY);
			xYResultados[1].y = Math.round(b2 + auxY);
			
		} else {
			
			xYResultados[0].y = Math.round(b + auxY);
			xYResultados[1].y = Math.round(b2 - auxY);
			
		}
		if (a > a2) {
			
			xYResultados[0].x = Math.round(a - auxX);
			xYResultados[1].x = Math.round(a2 + auxX);
			
		} else {
			
			xYResultados[0].x = Math.round(a + auxX);
			xYResultados[1].x = Math.round(a2 - auxX);
			
		}
		return xYResultados;

	}

	/**
	 * Verifica se o ponto (x,y) está contido nas coordenadas: 
	 * (xInicio, xFinal) e (yInicio, yFinal).
	 * @param xInicio inicio do escopo no eixo do x
	 * @param xFinal final do escopo no eixo do x
	 * @param yInicio inicio do escopo no eixo do y
	 * @param yFinal final do escopo no eixo do x
	 * @param x
	 * @param y
	 * @return
	 */
	public static boolean contem(int xInicio, int xFinal, int yInicio,
			int yFinal, int x, int y) {
		if (x >= xInicio && x <= xFinal && y >= yInicio && y <= yFinal) {
			return true;
		}
		return false;
	}
	
	/**
	 * Verifica se o ponto (x,y) está contido nas coordenadas
	 * dadas pelos pontos: letfTop e rightBottom.
	 * 
	 * @param leftTop ponto que guarda o inicio do escopo de x e y.
	 * @param rightBottom ponto que guarda o fim do escopo de x e y.
	 * @param x
	 * @param y
	 * @return
	 */
	public static boolean contem(Point leftTop, Point rightBottom, int x, int y) {
		return (x >= leftTop.getX() && x <= rightBottom.getX() && 
				y >= leftTop.getY() && y <= rightBottom.getY());
	}

	/**
	 * Dado dois pontos o método calcula o angulo (radianos) da reta no primeiro quadrante.
	 * Caso a reta tenha coefiente angular negativo a reta ser� espelhada.
	 * @param p1 primeiro ponto da reta
	 * @param p2 segundo ponto da reta
	 * @return o modulo do coefiente angular
	 */
	public static double anguloModularDeDoisPontos(Point p1, Point p2){
		if(p2.getX() == p1.getX()) return Math.PI/2;
		else return Math.atan(Math.abs(p2.getY()- p1.getY()+0.0) / Math.abs(p2.getX() - p1.getX()+0.0));
	}
	
	/**
	 * Retorna o ângulo (radianos) entre a reta que passa pelos dois pontos e o eixo do x. <br />
	 * Caso os dois ponto sejam idênticos não será possível passar uma reta entre eles.
	 * Nesse caso será retornado o valor: <b><code>Double.MIN_VALUE</code></b>.
	 * @param p1 ponto de origem da reta
	 * @param p2 ponto de destino da reta
	 * @return angulo em radianos.
	 */
	public static double anguloDeDoisPontos(Point p1, Point p2){
		double deltaX = p2.getX() - p1.getX();
		double deltaY = p2.getY() - p1.getY();
		return anguloDeDoisPontos(deltaX, deltaY);
	}

	/**
	 * Retorna o �ngulo (radianos) entre a reta e o eixo do x. <br />
	 * Caso os dois deltas sejam zero n�o ser� poss�vel forma uma reta.
	 * Nesse caso ser� retornado o valor: <b><code>Double.MIN_VALUE</code></b>.
	 * @param deltaX a diferen�a entre dos x's de dois pontos da reta
	 * @param deltaY a diferen�a entre dos y's de dois pontos da reta
	 * @return angulo da reta em rela��o ao eixo x (radianos).
	 */
	public static double anguloDeDoisPontos(double deltaX, double deltaY){
		if(deltaX>0){
			if(deltaY>0){
				return Math.atan(deltaY / deltaX);
			}else if(deltaY<0){
				return 2*Math.PI - Math.atan(Math.abs(deltaY) / deltaX);
			}else{
				return 0;
			}
		}else if(deltaX < 0){
			if(deltaY>0){
				return Math.PI-Math.atan(deltaY / Math.abs(deltaX));
			}else if(deltaY<0){
				return Math.PI+Math.atan(Math.abs(deltaY) / Math.abs(deltaX));
			}else{
				return Math.PI;
			}
		}else{
			if(deltaY>0){
				return Math.PI/2;
			}else if(deltaY<0){
				return 3*Math.PI/2;
			}else{
				return Double.MIN_VALUE;
			}
		}
	}
	
	/**
	 *Dado dois pontos a fun��o calcula o coeficiente angular da reta que passa pelos pontos.
	 * @param p1 primeiro ponto da reta
	 * @param p2 segundo ponto da reta
	 * @return O coeficiente angular da reta que passa por p1 e p2.
	 * @throws ArithmeticException caso o x do primeiro e segundo ponto sejam iguais
	 * a fun��o tenta realizar um divis�o por zero.
	 */
	public static double coeficienteA(Point p1, Point p2) throws ArithmeticException{
		if (p2.getX() == p1.getX()) throw new ArithmeticException("divisão por zero.");
		return (p2.getY() - p1.getY()+0.0) / (p2.getX() - p1.getX()+0.0);
	}
	
	/**
	 * Dado dois pontos para formar uma reta e o x de um ponto na reta 
	 * a fun��o retorna o y, tal que (x,y) pertence a reta.
	 * @param p1 primeiro ponto da reta
	 * @param p2 segundo ponto da reta
	 * @param x um inteiro qualquer
	 * @return o valor de y, tal que (x,y) pertence a reta.
	 */
	public static int retaY(Point p1, Point p2, int x){
		
		double cAngular = coeficienteA(p1, p2);
		double y = cAngular*(x - p1.getX()) + p1.getY();
		return (int)Math.round(y);
	}
	
	public static int retaY(Point p1, Point p2, int x, double cAngular){
		double y = cAngular*(x - p1.getX()) + p1.getY();
		return Math.round((float)y);
	}
	 
	/**
	 * Dado dois pontos para formar uma reta e o y de um ponto na reta 
	 * a função retorna o x, tal que (x,y) pertence a reta.
	 * @param p1 primeiro ponto da reta
	 * @param p2 segundo ponto da reta
	 * @param y um inteiro qualquer
	 * @return o valor de x, tal que (x,y) pertence a reta.
	 */
	public static int retaX(Point p1, Point p2, int y){
		try{
			double cAngular = coeficienteA(p1, p2);
			double x = (y - p1.getY())/cAngular + p1.getX();
			return (int)Math.round(x);
		}catch (ArithmeticException ae) {
			return p2.x;
		}
	}
	
	public static int retaX(Point p1, Point p2, int y, double cAngular){
			double x = (y - p1.getY())/cAngular + p1.getX();
			return (int)Math.round(x);
	}
	
	/**
	 * Calcula e retorna a dist�ncia entre dois pontos.
	 * @param p1 primeiro ponto
	 * @param p2 segundo ponto
	 * @return a dist�ncia entre os pontos p1 e p2.
	 */
	public static int distanciaEntreDoisPontos(Point p1, Point p2){
		return (int) Math.round(Math.sqrt(Math.pow(p1.getX()-p2.getX(), 2) + Math.pow(p1.getY()-p2.getY(), 2) ));
	}
	
	/**
	 * Dado dois pontos de uma reta <b>R</b> e um ponto de uma reta <b>S</b> perpendicular a <b>R</b>,
	 * a func�o retorna o ponto da intersec��o entre <b>R</b> e <b>S</b>.
	 * @param p1R primeiro ponto da reta <b>R</b>
	 * @param p2R segundo ponto da reta <b>R</b>
	 * @param pS ponto da reta <b>S</b>
	 * @return ponto de intersec��o entre as retas perpendiculares <b>R</b> e <b>S</b>
	 */
	public static Point pontoIntersec(Point p1R, Point p2R, Point pS){

		if(p1R.getY() == p2R.getY()) return new Point(pS.x, p1R.y);
		if(pS.equals(p1R)) return p1R;
		
		double coefAR;
		try{	
			coefAR = coeficienteA(p1R, p2R);
		}catch (ArithmeticException ae) {
			return new Point(p1R.x, pS.y);
		}

		double coefAS = (-1.0)/coefAR;

		double bS = pS.getY() - coefAS*pS.getX();
		double bR = p1R.getY() - coefAR*p1R.getX();

		Point resposta = new Point();

		resposta.x = (int)Math.round((bS-bR)/(coefAR - coefAS));
		resposta.y = (int)Math.round((bR*coefAS - bS*coefAR)/(coefAS-coefAR));
		return resposta;



	}
	
	public static int xDeUmaHipotenusa(double angulo, int hipotenusa){
		return Math.round(hipotenusa*((float)Math.cos(angulo)));
	}
	
	public static int yDeUmaHipotenusa(double angulo, int hipotenusa){
		return Math.round(hipotenusa*((float)Math.sin(angulo)));
	}
	
}
	
