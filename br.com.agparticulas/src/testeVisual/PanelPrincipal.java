package testeVisual;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class PanelPrincipal extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3421021007012408054L;
	
	private Point[] zombies;
	private Point[] zPosition;
	private boolean[] dead;
	
	private Point hero;
	private int moveTo;
	private int moveToV;
	private char fire;
	private Fires fires;
	
	private JProgressBar life;
	
	private MouseListener m = new MouseAdapter() {
		@Override
		public void mouseReleased(MouseEvent e) {
			mouseClick(e.getPoint());
		}
	};
	private KeyListener klistener = new KeyAdapter() {
		
		@Override
		public void keyPressed(KeyEvent e) {
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_D:
				moveTo = keyCode;
				break;

			case KeyEvent.VK_A:
				moveTo = keyCode;
				break;
				
			case KeyEvent.VK_W:
				moveToV = keyCode;
				break;
				
			case KeyEvent.VK_S:
				moveToV = keyCode;
				break;
				
			case KeyEvent.VK_RIGHT:
				fire = 'r';
				break;
				
			case KeyEvent.VK_LEFT:
				fire = 'l';
				break;
				
			case KeyEvent.VK_UP:
				fire = 'u';
				break;
				
			case KeyEvent.VK_DOWN:
				fire = 'd';
				break;
			}
		}
			
		public void keyReleased(KeyEvent e) {
			int keyCode = e.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_D:
				if(moveTo == KeyEvent.VK_D) moveTo = 0;
				break;

			case KeyEvent.VK_A:
				if(moveTo == KeyEvent.VK_A) moveTo = 0;
				break;
				
			case KeyEvent.VK_W:
				if(moveToV == KeyEvent.VK_W) moveToV = 0;
				break;
				
			case KeyEvent.VK_S:
				if(moveToV == KeyEvent.VK_S) moveToV = 0;
				break;
				
			case KeyEvent.VK_RIGHT:
				if(fire == 'r') fire = 'n';
				break;
				
			case KeyEvent.VK_LEFT:
				if(fire == 'l') fire = 'n';
				break;
				
			case KeyEvent.VK_UP:
				if(fire == 'u') fire = 'n';
				break;
				
			case KeyEvent.VK_DOWN:
				if(fire == 'd') fire = 'n';
				break;
				
			}
			
		};
	};
	
	
	
	
	public PanelPrincipal(Point[] zombies, Point hero, JProgressBar life) {
		this.zombies = new Point[zombies.length];
		this.zPosition = new Point[zombies.length];
		this.dead = new boolean[zombies.length];
		this.fire = 'n';
		this.fires = new Fires(500, 500);
		
		this.life = life;
		
		this.life.setValue(100);
		
		Random rnd =RandomGenerator.getRandomGenerator();
		for (int i = 0; i < zombies.length; i++) {
			this.zombies[i] = zombies[i];
			this.zPosition[i] = new Point(rnd.nextInt(500), rnd.nextInt(500));
			this.dead[i] = false;
		}
		
		this.hero = hero;
		this.start();
		
		super.addMouseListener(this.m);
		super.setFocusable(true);
		super.addKeyListener(this.klistener);
		
	}
	
	
	

	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		
		g.setColor(new Color(75, 0, 130));
		g.fillOval(this.hero.x-5, this.hero.y-5, 10, 10);
		g.setColor(Color.BLUE);
		
		
		for (int i=0; i<zPosition.length; i++) {
			if(!this.dead[i]){
				g.drawOval(zPosition[i].x-5, zPosition[i].y-5, 10, 10);
			}
		}
		
		g.setColor(Color.BLACK);
		fires.resetIterator();
		Point f;
		while(fires.hasNext()){
			f = fires.next();
			g.fillOval(f.x-2, f.y-2, 4, 4);
		}
//		for (Point mira : this.zombies) {
//			g.drawOval(mira.x-5, mira.y-5, 10, 10);
//		}
	}
	
	
	private void start(){
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				double ang = 0.0;
				while(true){
					for (int i = 0; i < zombies.length; i++) {
						ang = Matematica.anguloDeDoisPontos(zPosition[i], zombies[i]);
						
						if(Math.abs(zPosition[i].x - zombies[i].x) > 1 &&
								Math.abs(zPosition[i].y - zombies[i].y) > 1 ){
							
							zPosition[i].x += Matematica.xDeUmaHipotenusa(ang, 2);
							zPosition[i].y += Matematica.yDeUmaHipotenusa(ang, 2);
						}
						
						if(dead[i] == true) continue;
						
						if(Point.distance(zPosition[i].x, zPosition[i].y, 
								hero.x, hero.y) < 10){
							life.setValue(life.getValue()-1);
						}
						
					}
					
					switch (moveTo) {
					case KeyEvent.VK_D:
						hero.x+=2;
						break;
					case KeyEvent.VK_A:
						hero.x-=2;
						break;
					}

					switch (moveToV) {
					case KeyEvent.VK_W:
						hero.y-=2;
						break;
					case KeyEvent.VK_S:
						hero.y+=2;
						break;
					}
					
					
					
					if(fire != 'n'){
						fires.createFire(hero.x, hero.y, fire);
						fire = 'n';
					}
					
					fires.resetIterator();
					Point f;
					int indexFire = 0;
					while(fires.hasNext()){
						f = fires.next();
						for(int i = 0; i < zombies.length; i++){
							if(Point.distance(zPosition[i].x, zPosition[i].y, 
									f.x, f.y) < 6){
								dead[i] = true;
								indexFire = fires.indexOfFire(f);
							}
						}
					}
					
//					fires.removeFire(indexFire);
					
					fires.moveFires();
					
					repaint();
//					revalidate();
					try {
						Thread.sleep(94);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
				}
				
			}
		});
		t.start();
	}
	
	private void mouseClick(Point p){
		this.hero.x = p.x;
		this.hero.y = p.y;
	}
}
