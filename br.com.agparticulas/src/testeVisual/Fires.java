package testeVisual;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

public class Fires {

	private ArrayList<Point> fires;
	private ArrayList<Character> direction;
	private Iterator<Point> ite;
	private Rectangle size;
	private long id;
	private static long cont = 0;
	
	public Fires(int xSize, int ySize) {
		this.fires = new ArrayList<>();
		this.direction = new ArrayList<>();
		this.size = new Rectangle(xSize, ySize);
		Fires.cont++;
		this.id = Fires.cont;
	}
	
	public void createFire(int xInit, int yInit, char direction){
		this.fires.add(new Point(xInit, yInit));
		this.direction.add(new Character(direction));
	}
	
	public void moveFires(){
		int len = this.fires.size();
		for(int i=len-1; i>-1; i--){
			if(this.direction.get(i).equals(Character.valueOf('r'))){
				this.fires.get(i).x += 3;
			}else if(this.direction.get(i).equals(Character.valueOf('l'))){
				this.fires.get(i).x -= 3;
			}else if(this.direction.get(i).equals(Character.valueOf('u'))){
				this.fires.get(i).y -= 3;
			}else if(this.direction.get(i).equals(Character.valueOf('d'))){
				this.fires.get(i).y += 3;
			}
			
			if(!this.size.contains(this.fires.get(i))){
				this.fires.remove(i);
				this.direction.remove(i);
			}
		}
	}
	
	public void removeFire(int i){
		this.fires.remove(i);
		this.direction.remove(i);
	}
	
	public void resetIterator(){
		this.ite = this.fires.iterator();
	}
	
	public boolean hasNext(){
		return this.ite.hasNext();
	}
	
	public Point next(){
		return this.ite.next();
	}
	
	public int indexOfFire(Object fire){
		return this.fires.indexOf(fire);
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(arg0 instanceof Fires){
			return ((Fires)arg0).id == this.id;
		}else{
			return false;
		}
	}
	
}
