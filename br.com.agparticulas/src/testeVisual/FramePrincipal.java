package testeVisual;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.BevelBorder;

import bestPoint2D.floatChromosome.EPSO;
import bestPoint2D.floatChromosome.FloatChormosomeFac;
import bestPoint2D.floatChromosome.FloatFitnessMethod;
import bestPoint2D.floatChromosome.FloatWholeArithmetic;
import bestPoint2D.floatChromosome.FloatWholeEPSO;
import bestPoint2D.floatChromosome.FloatWholeEPSO2;
import bestPoint2D.floatChromosome.GaussianAVGBestMutation;
import bestPoint2D.floatChromosome.GaussianMutation;
import bestPoint2D.floatChromosome.RouletteWhell;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.EAStatisticDecorator;
import evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms.GeneticAlgorithm;
import evolucionaryAlgorithm.mainAlgorithm.recombinations.VoidRecombination;
import evolucionaryAlgorithm.mainAlgorithm.recombinations.SimpleCrossover;
import evolucionaryAlgorithm.mainAlgorithm.selectionMethods.TournamentSelection;
import evolucionaryAlgorithm.mainAlgorithm.selectionMethods.TournamentSelectionLi;
import evolucionaryAlgorithm.mainAlgorithm.selectionMethods.RouletteWheelSelection;
import evolucionaryAlgorithm.mainAlgorithm.survivalMethods.ElitistSurvive;
import evolucionaryAlgorithm.mainAlgorithm.survivalMethods.NoSurvive;
import evolucionaryAlgorithm.mainAlgorithm.mutationBehaviors.BinaryBitPerBitMutation;
import evolucionaryAlgorithm.mainAlgorithm.terminateConditions.BestFitness;
import evolucionaryAlgorithm.mainAlgorithm.terminateConditions.NeverEnds;


public class FramePrincipal extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1218960379281654221L;
	
	private Point hero;
	private Point[] zombies;
	private EvolucionaryAlgorithm ga;
	private Chromosome[] selec;
	
	
	private JComboBox comboSelecao;
	private JLabel jLabel1;
	private JPanel panelConfig;
	private JPanel panelGame;
	private JLabel jLabel2;
    private JProgressBar life;
	
        // Number of 'Ghost-zombies'
	private static final int NUM_GHOSTS = 36;
	
	public FramePrincipal() {
            // Hero position
            this.hero = new Point(250, 250);

            // Graph interface
            initComponents();

            // EPSO
            this.ga = new EAStatisticDecorator(new EPSO());

            // Fitness              [1] 
            this.ga.setFitnessMethod(new FloatFitnessMethod());
            
            // Selection            [4] 
            this.ga.setSelectionMethod(new RouletteWhell());        
            // this.ga.setSelectionMethod(new TournamentSelection(5)); // numTournamentSelect
            //this.ga.setSelectionMethod(new TournamentSelectionLi(5)); // numTournamentSelect
            //this.ga.setSelectionMethod(new RouletteWheelSelection());
            
            // Chromosome Factory   [1]
            this.ga.setPopulFactory(new FloatChormosomeFac());

            // Recombination        [5]
            //this.ga.setRecombination(new FloatWholeArithmetic(1.0f)); // probability
            //this.ga.setRecombination(new FloatWholeEPSO(1.0f)); // probability
            /*this.ga.setRecombination(new FloatWholeEPSO2(1.0f, 0.3f, 0.3f, 
                   0.4f)); // probability, a1, a2, a3*/
            this.ga.setRecombination(new VoidRecombination());
            //this.ga.setRecombination(new SimpleCrossover());

            // Mutation             [3]
            //this.ga.setMutationBehavior(new GaussianMutation(0.8f, 0.3f)); // probabily, SDeviation
            this.ga.setMutationBehavior(new GaussianAVGBestMutation(0.8f, 
                    0.3f, 0.3f, 0.3f, 0.4f)); // probabily, SDeviation, a1, a2, a3
            //this.ga.setMutationBehavior(new BinaryBitPerBitMutation(0.1f)); // probabily

            // Survival             [2]
            this.ga.setSurvivalMethod(new NoSurvive());
            //this.ga.setSurvivalMethod(new ElitistSurvive());

            // Terminate Condition  [2*]
            //this.ga.setTerminateCondition(new BestFitness(1)); // >= bestFitness
            this.ga.setTerminateCondition(new NeverEnds());

            this.zombies = new Point[NUM_GHOSTS];
            this.ga.initPopulation(NUM_GHOSTS, new Object[]{0.5f, 0.5f});
            this.selec = this.ga.getSelected();

            String[] xY;
            for (int i = 0; i < this.zombies.length; i++) {
                    xY = selec[i].toString().split(",");
                    this.zombies[i] = new Point(Math.round(Float.parseFloat(xY[0])*500),
                                    Math.round(Float.parseFloat(xY[1])*500));
            }

            JPanel panel = new PanelPrincipal(this.zombies, this.hero, this.life);
            panel.setSize(500, 500);
            panel.setPreferredSize(new Dimension(500, 500));
            panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            panel.setDoubleBuffered(true);
            this.panelGame.setLayout(new FlowLayout(FlowLayout.CENTER));
            this.panelGame.add(panel);

            /*
             * Thread responsável pelo funcionamento do algoritmo genético.
             */
            Thread t = new Thread(new Runnable() {

                    @Override
                    public void run() {
                            while(true){
                                    Object[] heroXY = {hero.x/500.0f, hero.y/500.0f};
                                    ga.selectChromosomes(heroXY);

                                    selec = ga.getSelected();

                                    String[] xY;
                                    for (int i = 0; i < zombies.length; i++) {
                                            xY = selec[i].toString().split(",");
                                            zombies[i].x = Math.round(Float.parseFloat(xY[0])*500);
                                            zombies[i].y = Math.round(Float.parseFloat(xY[1])*500);
                                    }
                                    try {
                                            Thread.sleep(705);
                                    } catch (InterruptedException e) {
                                            e.printStackTrace();
                                    }
                            }


                    }
            });

            t.start();
		
	}
	
	private void initComponents() {

	panelGame = new javax.swing.JPanel();
        panelConfig = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        comboSelecao = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        life = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelGame.setBorder(javax.swing.BorderFactory.createTitledBorder("Game"));
        panelGame.setPreferredSize(new java.awt.Dimension(530, 530));

        javax.swing.GroupLayout panelGameLayout = new javax.swing.GroupLayout(panelGame);
        panelGame.setLayout(panelGameLayout);
        panelGameLayout.setHorizontalGroup(
            panelGameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 518, Short.MAX_VALUE)
        );
        panelGameLayout.setVerticalGroup(
            panelGameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 507, Short.MAX_VALUE)
        );

        panelConfig.setBorder(javax.swing.BorderFactory.createTitledBorder("Configurações"));

        jLabel1.setText("Método de Seleção");

        comboSelecao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Roleta", "Torneio" }));
        comboSelecao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSelecaoActionPerformed(evt);
            }
        });

        jLabel2.setText("Life:");

        javax.swing.GroupLayout panelConfigLayout = new javax.swing.GroupLayout(panelConfig);
        panelConfig.setLayout(panelConfigLayout);
        panelConfigLayout.setHorizontalGroup(
            panelConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfigLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(comboSelecao, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelConfigLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(life, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelConfigLayout.setVerticalGroup(
            panelConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfigLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboSelecao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69)
                .addGroup(panelConfigLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(life, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(373, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelGame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelConfig, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelConfig, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelGame, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }
	
	private void comboSelecaoActionPerformed(java.awt.event.ActionEvent evt) {
        if(this.comboSelecao.getSelectedIndex() == 0){
        	this.ga.setSelectionMethod(new RouletteWhell());
        }else{
        	this.ga.setSelectionMethod(new TournamentSelectionLi(5));
        }
    }

}
