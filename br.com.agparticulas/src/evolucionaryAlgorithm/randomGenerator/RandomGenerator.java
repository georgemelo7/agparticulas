package evolucionaryAlgorithm.randomGenerator;

import java.util.Random;


public final class RandomGenerator {
	
	private RandomGenerator() {}
	
	private static Random rnd = new MersenneTwister();
	
	public static Random getRandomGenerator(){
		return RandomGenerator.rnd;
	}

}
