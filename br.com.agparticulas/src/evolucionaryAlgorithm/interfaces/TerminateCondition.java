package evolucionaryAlgorithm.interfaces;



public interface TerminateCondition {
	
	
	public boolean isTerminate(Chromosome[] population);

}
