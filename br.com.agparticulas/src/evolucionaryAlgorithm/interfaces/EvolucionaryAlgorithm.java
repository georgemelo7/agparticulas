package evolucionaryAlgorithm.interfaces;


/**
 * 
 * Interface de um algoritmo evolucionário. <br />
 * Três métodos básicos formam a base de um algoritmo evolucionário:<br /><br />
 * 
 * iniciar a população, selecionar individuos, e verificar a condição de parada.<br /><br />
 * 
 * Sendo que o método de selecionar indivíduos para a próxima geração envolve:<br /><br />
 * 
 * calcular fitness da população, selecionar indivíduos para cruzamento, cruzamento, mutação e sobrevivência.<br /><br />
 * 
 * Os métodos gets e sets permitem modificar em tempo de execução os comportamentos de cada método citado acima 
 * (Padrão Strategy).
 * 
 * E essa interface possibilita a utilização dos padrões Decorator e Proxy.
 * 
 * @author Gustavo
 *
 */
public interface EvolucionaryAlgorithm {
	
	
	public void initPopulation(int numChromosome, Object ... phenotype);
	
	public void selectChromosomes(Object ... phenotype);
	
	public boolean verifyTerminateCondition();
	
	
	public SurvivalMethod getSurvivalMethod();
	
	public void setSurvivalMethod(SurvivalMethod survivalMethod);
	
	public ChromosomeFactory getPopulFactory();
	
	public void setPopulFactory(ChromosomeFactory populFactory);
	
	public TerminateCondition getTerminateCondition();
	
	public void setTerminateCondition(TerminateCondition terminateCondition);

	public FitnessMethod getFitnessMethod();

	public void setFitnessMethod(FitnessMethod fitnessMethod);

	public SelectionMethod getSelectionMethod();

	public void setSelectionMethod(SelectionMethod selectionMethod);

	public Recombination getRecombination();

	public void setRecombination(Recombination recombination);

	public MutationBehavior getMutationBehavior();

	public void setMutationBehavior(MutationBehavior mutationBehavior);
	
	
	
	
	
	public Chromosome[] getPopulation();

	public Chromosome[] getSelected();

}
