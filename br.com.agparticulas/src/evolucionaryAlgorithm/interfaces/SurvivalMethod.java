package evolucionaryAlgorithm.interfaces;

/**
 * 
 * Interface pública para o método de sobrevivêcia de 
 * um algoritmo evolucionário.
 * 
 * A implementação dessa interface define qual o método 
 * de sobrevivência que será utilizado em um algoritmo evolucionário.<br />
 * 
 * @author Gustavo
 *
 */
public interface SurvivalMethod {
	
	
	/**
	 * Método resposável por realizar a sobrevivênvia da população.
	 * 
	 * @param children A prole da população
	 * @param population A população
	 */
	public void survive(Chromosome[] children, Chromosome[] population);

}
