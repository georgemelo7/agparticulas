package evolucionaryAlgorithm.interfaces;


public interface FitnessMethod {
	
	public int calcFitness(Chromosome chromosome, Object ... params);
	

}
