package evolucionaryAlgorithm.interfaces;


public interface MutationBehavior {
	
	public void mutate(Chromosome chromosome);
	
}
