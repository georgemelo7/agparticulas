package evolucionaryAlgorithm.interfaces;



public interface SelectionMethod {
	
	public void select(Chromosome[] population, Chromosome[] voidSelected);

}
