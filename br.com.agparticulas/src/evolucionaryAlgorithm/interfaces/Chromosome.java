package evolucionaryAlgorithm.interfaces;



/**
 * 
 * Classe representa um Cromossomo.
 * Herda um fitness (int) e um likelihood (float) para ordenar os cromossos.
 * 
 * @author Gustavo
 *
 */
public abstract class Chromosome implements Comparable<Chromosome>{


	protected int fitness;
	protected float likelihood;
	
	
	/**
	 * No-args construtor. Define fitness e likelihood como zero.
	 */
	public Chromosome(){
		this.fitness = 0;
		this.likelihood = 0.0f;
	}
	
	
	/**
	 * Dependendo da Representação interna do cromossomo esse método retorna 
	 * o número de genes do cromossomo.
	 * 
	 * @return O número de genes.
	 */
	public abstract int getNumGenes();
	
	/**
	 * Modifica a codificação do genes, ou seja, define novos alelos para esse cromossomo.
	 *  
	 * @param alleles Os novos alelos para esse cromossomo. 
	 */
	public abstract void setGenes(String alleles);
	
	
	/**
	 * Retorna os alelos atuais desse cromossomo em um String.
	 * 
	 * @return Os alelos do cromossomo em String.
	 */
	public abstract String toString();
	
	
	/**
	 * Limpa os alelos do Cromossomo para um valor Default.
	 */
	protected abstract void clearChromosome();
	
	
	/**
	 * Retorna o valor do Fitness desse cromossomo.
	 * 
	 * @return Valor do Fitness.
	 */
	public int getFitness() {
		return fitness;
	}

	/**
	 * Define um fitness para esse Cromossomo.
	 * @param fitness O novo valor do Fitness.
	 */
	public void setFitness(int fitness) {
		this.fitness = fitness;
	}
	
	/**
	 * Retorna o valor do Likelihood desse cromossomo. <br />
	 * 
	 * <b>Obs:</b> Likelihood funciona como o fitness, mas guarda um valor float. 
	 * 
	 * @return Valor do Likelihood.
	 */
	public float getLikelihood(){
		return this.likelihood;
	}
	
	/**
	 * Define o valor do Likelihood desse cromossomo. <br />
	 * 
	 * <b>Obs:</b> Likelihood funciona como o fitness, mas guarda um valor float. 
	 * @param likelihood O novo valor do Likelihood.
	 */
	public void setLikelihood(float likelihood){
		this.likelihood = likelihood;
	}
	
	
	/**
	 * Define um valor Default para o Fitness e para o Likelihood do Cromossomo.
	 */
	public void clear(){
		this.likelihood = 0;
		this.fitness = 0;
		this.clearChromosome();
	}
	
	
	/**
	 * Compara dois cromossomos através do seu fitness.
	 * 
	 * @return <b>-1</b> se this.fitness < o.fitness; <b>1</b> se this.fitness > o.fitness; <b>0</b> se this.fitness = o.fitness
	 */
	@Override
	public int compareTo(Chromosome o) {
		if(this.fitness < o.getFitness()){
			return -1;
		}else if (this.fitness > o.getFitness()){
			return 1;
		}
		return 0;
	}
	
	
}
