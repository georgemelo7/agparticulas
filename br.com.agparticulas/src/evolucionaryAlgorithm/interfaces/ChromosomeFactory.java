package evolucionaryAlgorithm.interfaces;


/**
 * 
 * Padrão Abstract Factory para a criação de instâncias de {@link Chromosome}
 * Ajuda métodos abstratos a instanciar Cromosomos mesmo sem saber seu tipo.
 * 
 * As classes que implemetarem essa interface deve usar um tipo não abstrato de 
 * um cromossomo. 
 * 
 * @author Gustavo
 *
 */
public interface ChromosomeFactory {
	
	/**
	 * Cria uma população inicial dado o número de cromossomos.
	 * 
	 * @param numChromosome Número de cromossomos a serem criados.
	 * @return Uma referência a um array de Cromossomos
	 */
	public Chromosome[] createInitalPopulation(int numChromosome);
	
	/**
	 * Cria um array de Cromossomos vazios.
	 * 
	 * @param numChromosome tamanho do Array
	 * @return
	 */
	public Chromosome[] createVoidPopulation(int numChromosome);
	
	/**
	 * Cria um cromossomo de forma aleatória.
	 * 
	 * @return Um cromossomo criado aleatoriamente.
	 */
	public Chromosome createRandomChromosome();
	
	/**
	 * Instancia um cromossomo com os alelos passados na forma 
	 * de uma String.
	 * 
	 * @param alleles Alelos do cromossomo.
	 * @return Uma referência a um cromossomo com os genes definidos pelos alelos
	 * passados por parâmetro.
	 */
	public Chromosome createChromosome(String alleles);
	
	/**
	 * Define um valor default para os genes de todos os cromossomos de um array.
	 * 
	 * @param chromosomes Array de cromossomos a serem definidos com valores default.
	 */
	public void clearChromosomes(Chromosome[] chromosomes);
	
}
