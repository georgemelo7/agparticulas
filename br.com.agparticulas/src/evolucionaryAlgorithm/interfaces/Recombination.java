package evolucionaryAlgorithm.interfaces;



public interface Recombination {
	
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory);
	
}
