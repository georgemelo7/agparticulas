package evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.interfaces.FitnessMethod;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.interfaces.SurvivalMethod;
import evolucionaryAlgorithm.interfaces.TerminateCondition;

public class EAStatisticDecorator implements EvolucionaryAlgorithm{

	private EvolucionaryAlgorithm algorithm;
	private int geracao;
	
	public EAStatisticDecorator(EvolucionaryAlgorithm algorithm) {
		this.algorithm = algorithm;
	}
	
	
	@Override
	public void initPopulation(int numChromosome, Object ... args) {
		System.out.println("-----------------------  StatisticDecorator  ------------------------");
		System.out.println("SD-> Número de indivíduos: "+numChromosome);
		System.out.println("SD-> Fitness: "+ this.algorithm.getFitnessMethod().toString());
		System.out.println("SD-> Seleção: "+ this.algorithm.getSelectionMethod().toString());
		System.out.println("SD-> Recombinação: "+ this.algorithm.getRecombination().toString());
		System.out.println("SD-> Mutação: "+ this.algorithm.getMutationBehavior().toString());
		System.out.println("SD-> Condição de Parada: "+this.algorithm.getTerminateCondition().toString());
		this.algorithm.initPopulation(numChromosome, args);
		this.geracao = 0;
		
		System.out.println("'Geração' 'Média dos Fitness' 'Desvio Padrão do Fitness'"+
				" 'Melhor Fitness' 'Melhor Indivíduo'");
	}

	@Override
	public void selectChromosomes(Object ... phenotype) {
		
		this.algorithm.selectChromosomes(phenotype);
		
		Chromosome[] popul = this.algorithm.getPopulation();
		
		int iMaior = 0;
		float sum =0, maiorF=0,fitness;
		double medP, varP=0, devP;
		
		
		for (int i = 0; i < popul.length; i++) {
			fitness = popul[i].getLikelihood();
			sum += fitness;
			if(fitness > maiorF){
				maiorF = fitness;
				iMaior = i;
			}
		}
		medP = sum*1.0f/popul.length;
		
		for (int i = 0; i < popul.length; i++) {
			varP += Math.pow(popul[i].getLikelihood()-medP, 2);
		}
		varP = varP/popul.length;
		devP = Math.sqrt(varP);
		
		String[] melhor = popul[iMaior].toString().split(",");
		
		System.out.println(this.geracao+" "+medP+" "+devP+" "+
		maiorF+" "+melhor[0]+" "+melhor[1]);
		
		
		this.geracao++;
	}

	@Override
	public boolean verifyTerminateCondition() {
		return this.algorithm.verifyTerminateCondition();
	}

	
	
	@Override
	public SurvivalMethod getSurvivalMethod() {
		return this.algorithm.getSurvivalMethod();
	}
	
	@Override
	public void setSurvivalMethod(SurvivalMethod survivalMethod) {
		this.algorithm.setSurvivalMethod(survivalMethod);
	}
	
	
	@Override
	public ChromosomeFactory getPopulFactory() {
		return this.algorithm.getPopulFactory();
	}

	@Override
	public void setPopulFactory(ChromosomeFactory populFactory) {
		this.algorithm.setPopulFactory(populFactory);
	}

	@Override
	public TerminateCondition getTerminateCondition() {
		return this.algorithm.getTerminateCondition();
	}

	@Override
	public void setTerminateCondition(TerminateCondition terminateCondition) {
		this.algorithm.setTerminateCondition(terminateCondition);
	}

	@Override
	public FitnessMethod getFitnessMethod() {
		return this.algorithm.getFitnessMethod();
	}

	@Override
	public void setFitnessMethod(FitnessMethod fitnessMethod) {
		this.algorithm.setFitnessMethod(fitnessMethod);
	}

	@Override
	public SelectionMethod getSelectionMethod() {
		return this.algorithm.getSelectionMethod();
	}

	@Override
	public void setSelectionMethod(SelectionMethod selectionMethod) {
		this.algorithm.setSelectionMethod(selectionMethod);
	}

	@Override
	public Recombination getRecombination() {
		return this.algorithm.getRecombination();
	}

	@Override
	public void setRecombination(Recombination recombination) {
		this.algorithm.setRecombination(recombination);
	}

	@Override
	public MutationBehavior getMutationBehavior() {
		return this.algorithm.getMutationBehavior();
	}

	@Override
	public void setMutationBehavior(MutationBehavior mutationBehavior) {
		this.algorithm.setMutationBehavior(mutationBehavior);
	}

	@Override
	public Chromosome[] getPopulation() {
		return this.algorithm.getPopulation();
	}

	@Override
	public Chromosome[] getSelected() {
		return this.algorithm.getSelected();
	}

}
