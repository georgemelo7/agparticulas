package evolucionaryAlgorithm.mainAlgorithm.evolAlgorithms;


import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.EvolucionaryAlgorithm;
import evolucionaryAlgorithm.interfaces.FitnessMethod;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.interfaces.SurvivalMethod;
import evolucionaryAlgorithm.interfaces.TerminateCondition;

public class GeneticAlgorithm implements EvolucionaryAlgorithm {
	
	
	
	private ChromosomeFactory factory;
	private FitnessMethod fitnessMethod;
	private SelectionMethod selectionMethod;
	private Recombination recombination;
	private MutationBehavior mutationBehavior;
	private SurvivalMethod survivalMethod;
	private TerminateCondition terminateCondition;
	private Chromosome[] population;
	private Chromosome[] selected;
	private Chromosome[] chromosomePair;
	private Chromosome[] children;
	
	
	public void initPopulation(int numChromosome, Object ... phenotype ){
		this.selected =  this.factory.createInitalPopulation(numChromosome);
		this.population = this.factory.createVoidPopulation(numChromosome);
		this.chromosomePair = this.factory.createVoidPopulation(2);
		this.children = this.factory.createVoidPopulation(2);
		for (int i = 0; i < this.selected.length; i++) {
			this.fitnessMethod.calcFitness(selected[i], phenotype);
		}
	}
	
	public void selectChromosomes(Object ... phenotype){

		for (int i = 0; i < this.population.length; i++) {
			this.population[i].setGenes(this.selected[i].toString());
			this.population[i].setFitness(this.selected[i].getFitness());
			this.population[i].setLikelihood(this.selected[i].getLikelihood());
		}
		
		
		
		for(int i = 0; i < population.length; i++){
			
			this.selectionMethod.select(population, chromosomePair);	
			this.recombination.recombination(chromosomePair, children, this.factory);
			this.selected[i].setGenes(children[0].toString());
			this.mutationBehavior.mutate(this.selected[i]);
			
			try{
				i++;
				this.selected[i].setGenes(children[1].toString());
				this.mutationBehavior.mutate(this.selected[i]);
				
			}catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("ArrayIndexOutOfBoundsException");
				break;
			}

		}
		
		for (int i = 0; i < population.length; i++) {
			this.fitnessMethod.calcFitness(population[i],phenotype);
		}
		
		this.survivalMethod.survive(this.selected, this.population);
		
	}
	

	
	public boolean verifyTerminateCondition(){
		return this.terminateCondition.isTerminate(this.population);
	}
	
	
	
	
	
	public SurvivalMethod getSurvivalMethod() {
		return survivalMethod;
	}

	public void setSurvivalMethod(SurvivalMethod survivalMethod) {
		this.survivalMethod = survivalMethod;
	}

	public ChromosomeFactory getPopulFactory() {
		return factory;
	}
	public void setPopulFactory(ChromosomeFactory populFactory) {
		this.factory = populFactory;
	}
	public TerminateCondition getTerminateCondition() {
		return terminateCondition;
	}
	public void setTerminateCondition(TerminateCondition terminateCondition) {
		this.terminateCondition = terminateCondition;
	}

	public FitnessMethod getFitnessMethod() {
		return fitnessMethod;
	}

	public void setFitnessMethod(FitnessMethod fitnessMethod) {
		this.fitnessMethod = fitnessMethod;
	}

	public SelectionMethod getSelectionMethod() {
		return selectionMethod;
	}

	public void setSelectionMethod(SelectionMethod selectionMethod) {
		this.selectionMethod = selectionMethod;
	}

	public Recombination getRecombination() {
		return recombination;
	}

	public void setRecombination(Recombination recombination) {
		this.recombination = recombination;
	}

	public MutationBehavior getMutationBehavior() {
		return mutationBehavior;
	}

	public void setMutationBehavior(MutationBehavior mutationBehavior) {
		this.mutationBehavior = mutationBehavior;
	}

	public Chromosome[] getPopulation() {
		return population;
	}

	public Chromosome[] getSelected() {
		return selected;
	}
	
	
	
	
	

}
