package evolucionaryAlgorithm.mainAlgorithm.recombinations;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class SimpleCrossover implements Recombination {
	
	
	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children, ChromosomeFactory factory) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		
		if(parents[0].getNumGenes() == parents[0].getNumGenes()){
			int part = rnd.nextInt(parents[0].getNumGenes());
			String rep1 = parents[0].toString().substring(0, part) + parents[1].toString().substring(part);
			String rep2 = parents[1].toString().substring(0, part) + parents[0].toString().substring(part);
			children[0] = factory.createChromosome(rep1);
			children[1] = factory.createChromosome(rep2);
			
		}
		
	}
	
	@Override
	public String toString() {
		return "Simple Crossover";
	}

	
}
