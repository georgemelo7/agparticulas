package evolucionaryAlgorithm.mainAlgorithm.recombinations;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.interfaces.Recombination;

public class VoidRecombination implements Recombination {

	@Override
	public void recombination(Chromosome[] parents, Chromosome[] children,
			ChromosomeFactory factory) {
		
		for (int i = 0; i < children.length; i++) {
			children[i].setGenes(parents[i].toString());
		}
	}

}
