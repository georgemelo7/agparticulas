package evolucionaryAlgorithm.mainAlgorithm.fitnessMethods;


import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.FitnessMethod;
import evolucionaryAlgorithm.mainAlgorithm.chromosomes.BinaryChromosome;

public class FitnessTest implements FitnessMethod {

	private Chromosome chr;
	
	public FitnessTest() {
		this.chr = new BinaryChromosome("0101010010");
	}
	
	@Override
	public int calcFitness(Chromosome chromosome, Object ... params) {
		
		
		
		int aux = Integer.parseInt(chromosome.toString(), 2);
		int num = Integer.parseInt(this.chr.toString(), 2);
		
//		System.out.println("t:" + num+ "chr: "+aux);
		
		return 1023 - Math.abs(aux - num);
	}
	
	@Override
	public String toString() {
		return "Compare Phenotipes of binary Chromosomes: "+ chr.toString();
	}

}
