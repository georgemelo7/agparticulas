package evolucionaryAlgorithm.mainAlgorithm.survivalMethods;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.SurvivalMethod;

public class NoSurvive implements SurvivalMethod {

	@Override
	public void survive(Chromosome[] children, Chromosome[] population) {
		//Nada acontece
	}

}
