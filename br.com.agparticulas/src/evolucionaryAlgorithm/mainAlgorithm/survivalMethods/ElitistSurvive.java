package evolucionaryAlgorithm.mainAlgorithm.survivalMethods;

import java.util.Arrays;
//import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.SurvivalMethod;
//import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class ElitistSurvive  implements SurvivalMethod{
	
	private int numSurvivors;
	
	
	public ElitistSurvive() {
		this.numSurvivors = 3;
	}
	
	public ElitistSurvive(int numSurvivors){
		this.numSurvivors = numSurvivors;
	}
	
	@Override
	public void survive(Chromosome[] children, Chromosome[] population) {
		
		
		Arrays.sort(population);
		Arrays.sort(children);
		
		
//		Random rnd = RandomGenerator.getRandomGenerator();
		
		for (int i = 0; i < this.numSurvivors; i++) {
			children[i].setGenes(population[population.length-i-1].toString());
			children[i].setLikelihood(population[population.length-i-1].getLikelihood());
			children[i].setFitness(population[population.length-i-1].getFitness());
		}
	}

}
