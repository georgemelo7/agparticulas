package evolucionaryAlgorithm.mainAlgorithm.selectionMethods;

import java.util.ArrayList;
import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;

public class TournamentSelectionLi implements SelectionMethod {

	
	private int numTournamentSelect;
	
	public TournamentSelectionLi(int numTournamentSelect) {
		this.numTournamentSelect = numTournamentSelect;
	}
	
	
	@Override
	public void select(Chromosome[] population, Chromosome[] selected) {
		Random rnd = RandomGenerator.getRandomGenerator();
		ArrayList<Integer> sele = new ArrayList<>();
		int num, maior = 0, maior2 = 0;
		
		for (int i = 0; i < this.numTournamentSelect; i++) {
			num = rnd.nextInt(population.length);
//			sele.add(num);
//			num = rnd.nextInt(population.length);
			
			while(sele.contains(Integer.valueOf(num))){
				num = rnd.nextInt(population.length);
			}
			sele.add(Integer.valueOf(num));
		}
		for (Integer i : sele) {
			
			if(population[i].getLikelihood() > population[maior].getLikelihood()){
				maior2 = maior;
				maior = i;	
			}else if(population[i].getLikelihood() > population[maior2].getLikelihood()){
				maior2 = i;
			}
		}
		
		
		if(selected.length != 2){
			selected = new Chromosome[2];
		}
		selected[0].setGenes(population[maior].toString()) ;
		selected[1].setGenes(population[maior2].toString());
		
	}

}
