package evolucionaryAlgorithm.mainAlgorithm.selectionMethods;

import java.util.ArrayList;
import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class TournamentSelection implements SelectionMethod{

	private int numTournamentSelect;
	
	public TournamentSelection(int numTournamentSelect) {
		this.numTournamentSelect = numTournamentSelect;
	}
	
	
	@Override
	public void select(Chromosome[] population, Chromosome[] selected) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		ArrayList<Integer> sele = new ArrayList<>();
		int num, maior = 0, maior2 = 0;
		
		for (int i = 0; i < this.numTournamentSelect; i++) {
			num = rnd.nextInt(population.length);
			while(sele.contains(Integer.valueOf(num))){
				num = rnd.nextInt(population.length);
			}
			sele.add(Integer.valueOf(num));
		}
		for (Integer i : sele) {
			if(population[i].getFitness() > population[maior].getFitness()){
				maior2 = maior;
				maior = i;	
			}else if(population[i].getFitness() > population[maior2].getFitness()){
				maior2 = i;
			}
		}
		
		if(selected.length != 2){
			selected = new Chromosome[2];
		}
		selected[0] = population[maior];
		selected[1] = population[maior2];
	}
	
	@Override
	public String toString() {
		return "Torneio - 2 melhores entre "+ this.numTournamentSelect+" aleatórios.";
	}

}
