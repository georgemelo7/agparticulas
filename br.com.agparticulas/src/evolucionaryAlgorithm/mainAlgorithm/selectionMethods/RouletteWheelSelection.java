package evolucionaryAlgorithm.mainAlgorithm.selectionMethods;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.SelectionMethod;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class RouletteWheelSelection implements SelectionMethod {

	@Override
	public void select(Chromosome[] population, Chromosome[] voidSelected) {
		
		Random rnd = RandomGenerator.getRandomGenerator();
		
		int c1=0, c2=0;
		
		float num = rnd.nextFloat();
		
		System.out.println("1 numero sorteado: "+num);
		
		for (int i = 0; i < population.length; i++) {
			if(population[i].getLikelihood() >= num){
				c1 = i;
				System.out.println("likelihood: "+ population[i].getLikelihood());
				break;
			}
		}
		
		


		do{
			num = rnd.nextFloat();
			System.out.println("2 numero sorteado: "+num);
			for (int i = 0; i < population.length; i++) {
				if(population[i].getLikelihood() >= num){
					System.out.println("likelihood: "+ population[i].getLikelihood());
					c2 = i;
					break;
				}
			}
		}while(c2 == c1);


		if(voidSelected.length != 2){
			voidSelected = new Chromosome[2];
		}
		voidSelected[0] = population[c1];
		voidSelected[1] = population[c2];
	}

}
