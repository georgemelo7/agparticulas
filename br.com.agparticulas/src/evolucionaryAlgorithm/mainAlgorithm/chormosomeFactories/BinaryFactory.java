package evolucionaryAlgorithm.mainAlgorithm.chormosomeFactories;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.ChromosomeFactory;
import evolucionaryAlgorithm.mainAlgorithm.chromosomes.BinaryChromosome;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class BinaryFactory implements ChromosomeFactory{
	
	
	private Random randomGenerator;
	private int numGenes;
	
	public BinaryFactory(int numGenes) {
		this.randomGenerator = RandomGenerator.getRandomGenerator();
		this.numGenes = numGenes;
	}
	
	
	public Chromosome[] createInitalPopulation(int numChromosome){
		Chromosome[] chromosomes = new Chromosome[numChromosome];
		for(int i=0; i<numChromosome; i++){
			chromosomes[i] = this.createRandomChromosome();
		}
		return chromosomes;
	}
	
	
	public Chromosome createRandomChromosome(){
		StringBuffer bits = new StringBuffer();
		for(int i=0; i<this.numGenes; i++){
			if(randomGenerator.nextDouble() >= 0.5){
				bits.append("1");
			}else{
				bits.append("0");
			}
		}
		
		return new BinaryChromosome(bits.toString());
	}
	
	@Override
	public Chromosome[] createVoidPopulation(int numChromosome) {
		
		Chromosome[] pop = new BinaryChromosome[numChromosome];
		for (int i = 0; i < pop.length; i++) {
			pop[i] = this.createChromosome("");
		}
		
		return pop;
	}
	
	public Chromosome createChromosome(String str){
		return new BinaryChromosome(str);
		
	}
	
	@Override
	public void clearChromosomes(Chromosome[] chromosomes) {
		for (Chromosome chromosome : chromosomes) {
			chromosome.clear();
		}
	}

}
