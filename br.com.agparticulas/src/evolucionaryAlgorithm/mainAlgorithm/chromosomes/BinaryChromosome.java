package evolucionaryAlgorithm.mainAlgorithm.chromosomes;

import evolucionaryAlgorithm.interfaces.Chromosome;

public class BinaryChromosome extends Chromosome{

	
	private boolean[] genes;
	
	
	public BinaryChromosome(String bits) {
		this.genes = new boolean[bits.length()];
		setGenes(bits);
	}
	
	
	@Override
	public void setGenes(String bits) {
		
		for(int i = 0; i < this.genes.length; i++){
			if(bits.charAt(i) == '0'){
				this.genes[i] = true;
			}else if(bits.charAt(i) == '1'){
				this.genes[i] = false;
			}else{
				System.out.println("Bit Inválido");
			}
		}
		
	}
	
	
	@Override
	public int getNumGenes() {
		return genes.length;
	}
	
	@Override
	protected void clearChromosome() {
		for (int i = 0; i < this.genes.length; i++) {
			this.genes[i] = true;
		}
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Chromosome){
			Chromosome chrom = (Chromosome) obj;
			if(this.getNumGenes() == chrom.getNumGenes() && this.genes.toString().equals(chrom.toString())){
				return true;
			}
		}
		return false;
	}
	
	
	
	@Override
	public String toString() {
		StringBuffer chromosome = new StringBuffer();
		for(boolean gene : this.genes ){
			if(gene){
				chromosome.append("0");
			}else{
				chromosome.append("1");
			}
			
		}
		
		return chromosome.toString();
	}


	






	
	

}
