package evolucionaryAlgorithm.mainAlgorithm.mutationBehaviors;

import java.util.Random;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.MutationBehavior;
import evolucionaryAlgorithm.randomGenerator.RandomGenerator;


public class BinaryBitPerBitMutation implements MutationBehavior {

	private float probabily;
	
	public BinaryBitPerBitMutation(float probabily) {
		this.probabily = probabily;
	}
	
	
	@Override
	public void mutate(Chromosome chromosome) {
		
		StringBuffer chromo = new StringBuffer(chromosome.toString());
		int len = chromo.length();
		Random rnd = RandomGenerator.getRandomGenerator();
		
		for (int i = 0; i < len; i++) {
			if(rnd.nextFloat() < this.probabily){
//				System.out.println("Mutação ocorreu no bit " + i);
				if(chromo.charAt(i) == '1'){
					chromo.setCharAt(i, '0');
				}else if(chromo.charAt(i) == '0'){
					chromo.setCharAt(i, '1');
				}
			}
		}
		
		chromosome.setGenes(chromo.toString());
	}
	
	@Override
	public String toString() {
		return "Mutação binária bit a bit: "+ this.probabily;
	}
	
}
