package evolucionaryAlgorithm.mainAlgorithm.terminateConditions;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.TerminateCondition;

public class NeverEnds implements TerminateCondition{

	@Override
	public boolean isTerminate(Chromosome[] population) {
		return false;
	}

}
