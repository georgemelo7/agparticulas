package evolucionaryAlgorithm.mainAlgorithm.terminateConditions;

import evolucionaryAlgorithm.interfaces.Chromosome;
import evolucionaryAlgorithm.interfaces.TerminateCondition;

public class BestFitness implements TerminateCondition{

	
	private int bestFitness;
	
	public BestFitness(int bestFitness) {
		this.bestFitness = bestFitness;
	}
	
	
	@Override
	public boolean isTerminate(Chromosome[] population) {
		
		for (Chromosome chromosome : population) {
			if (chromosome.getFitness() >= this.bestFitness) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Termina quando acha o melhor fitness: " + this.bestFitness;
	}

}
